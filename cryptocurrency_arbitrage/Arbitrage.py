# -*- coding: utf-8 -*-
"""
This file is the core of the system
This module performs the arbitrage operation of the coins.

Example:
        This file can be tested by using modules/newbots.py file in web project
        $ python modules/newbots.py 
        The file reads input from command line/ from a subprocess command of python                                                     
Todo:
    * Remove already identified unwanted lines of code with hardcodes
    * Check the logic of arbitrage finding

"""

from app.markets.Constants import *
from app.markets.ErrorConstants import *

import datetime
import string
import random
import sys

from app.markets.GeminiApi import *
from app.markets.GdaxApi import *
from app.markets.KrakenApi import *

from app.markets.ExceptionResolver import *

# Start of setting Obtaining logging credentials for exchanges

gapikey = str(sys.argv[1])
gapisecret = str(sys.argv[2])
gapipass = str(sys.argv[3])

geminikey = str(sys.argv[4])
geminisecret = str(sys.argv[5])

krakenkey = str(sys.argv[6])
krakensecret = str(sys.argv[7])

tollerance = float(sys.argv[8])
tollerance = tollerance / 100  # Since tollerance is entered as a percentage, we convert it into decimal for calculation

amount = float(sys.argv[9])

# Which exchanges to trade with

useKraken = bool(sys.argv[10] == "true")
useGemini = bool(sys.argv[11] == "true")
useGdax = bool(sys.argv[12] == "true")

print(useKraken)
print(useGemini)
print(useGdax)

# Setting trading fee for different exchanges


feeKraken = float(sys.argv[13])
feeGemini = float(sys.argv[14])
feeGdax = float(sys.argv[15])

# Setting interval for running the arbitrage
runInterval = float(sys.argv[16])

# Setting test mode
testMode = bool(sys.argv[17] == "true")

# FIX: What is opportunity
opportunity = float(sys.argv[18])

# Debugginginfo
print("TEST")
print(testMode)
print(feeKraken)
print(feeGemini)
print(feeGdax)

# Building the api object for all exchanges with required authentication credentials
gdaxApi = GdaxApi(gapikey, gapisecret, gapipass)
geminiApi = GeminiApi(geminikey, geminisecret, isSandbox=True)
krakenApi = KrakenApi(krakenkey, krakensecret)

print(sys.argv)
print("Created apis")
print(tollerance)
print(amount)


# This is the nonce that is sent with every request for geminiApi
# FIXME: This code has to be adjusted to remember previous nonce and increment the nonce every  request

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


# Constructs required to increase store api and fee information for future
apiArray = []
nameArray = []
feeArray = []

if (useKraken):
    apiArray.insert(0, krakenApi)
    nameArray.insert(0, "Kraken")
    feeArray.insert(0, feeKraken)
if (useGemini):
    apiArray.insert(0, geminiApi)
    nameArray.insert(0, "Gemini")
    feeArray.insert(0, feeGemini)
if (useGdax):
    apiArray.insert(0, gdaxApi)
    feeArray.insert(0, feeGdax)
    nameArray.insert(0, "Gdax")


def arbitrage():
    priceArray = []

    try:

        # Get price from all exchanges
        print("Checking kraken price")
        krakenBTCprice = float(krakenApi.getTicker('XXBTZUSD'))
        print("krakenBTCprice is ", krakenBTCprice)

        print("Checking gdax price")
        gdaxBTCprice = (float)(gdaxApi.getTicker('BTC-USD'))
        print("gdaxprice is ", gdaxBTCprice)

        print("Checking gemini price")
        # geminiBTCprice = float(geminiApi.get_ticker('btcusd')['last'])
        geminiBTCprice = geminiApi.getTicker('btcusd')
        print("gemini price is ", geminiBTCprice)





    except Exception, ex:
        raise ExchangeException.ExchangeException(ErrorConstants.TICKER_ERROR)

    # store infromation of prices of bitcoin now in an array in the same order as API Array
    if (useKraken):
        priceArray.insert(0, krakenBTCprice)

    if (useGemini):
        priceArray.insert(0, geminiBTCprice)

    if (useGdax):
        priceArray.insert(0, gdaxBTCprice)

    # Find the maximum price and minimum price offered by all exchanges
    maxPrice = max(priceArray)
    maxIndex = priceArray.index(maxPrice)
    minPrice = min(priceArray)
    minIndex = priceArray.index(minPrice)

    # debugging information
    print("price array is", priceArray)
    print("index of maximum price coin is ", maxIndex)
    print("index of minimum price coin is ", minIndex)

    # threshold is used to calculate the limit to place the order
    threshold = (minPrice / 100) * tollerance

    print("tollerance is", tollerance)
    print("minprice is ", minPrice)
    print("maxprice is ", maxPrice)

    buythreshold = round((minPrice * (1 + tollerance)), 2)
    sellthreshold = round((maxPrice * (1 - tollerance)), 2)

    # For new calculation
    Delta = sellthreshold - buythreshold
    print("Delta ", Delta)
    arbitrageProfitPercent = (Delta / ((sellthreshold + buythreshold) / 2)) * 100
    print("arbitrageProfitPercent", arbitrageProfitPercent)
    TotalFee = feeArray[maxIndex] + feeArray[minIndex]
    print("TotalFee", TotalFee)
    print (arbitrageProfitPercent - TotalFee > opportunity)
    # End of new calculation

    if arbitrageProfitPercent - TotalFee > opportunity:

        ts = time.time()
        # Map to hold the buy details
        buyDetails = {}
        buyDetails['name'] = nameArray[minIndex]
        buyDetails['coin'] = Constants.bitcoin
        buyDetails['amount'] = str(amount)

        # Map to hold to the sell details
        sellDetails = {}
        sellDetails['name'] = nameArray[maxIndex]
        sellDetails['coin'] = Constants.bitcoin
        sellDetails['amount'] = str(amount)


        # ts = time.time()
        f = open('./orderlist.json', 'a')
        f.write("{\"data\":")
        f.write("\"")
        f.write(datetime.datetime.fromtimestamp(ts).strftime('%a %b %d %Y %X&%z'))
        f.write(nameArray[maxIndex] + ":" + str(maxPrice) + "&" + nameArray[minIndex] + ":" + str(minPrice) + "&")
        f.write(str(maxPrice - minPrice) + "&")
        f.write(str(amount) + "&")
        f.write(str((maxPrice - threshold - minPrice - threshold) / ((maxPrice + minPrice) / 2) * 100) + "&")
        f.write(str(
            (maxPrice - threshold - minPrice - threshold) / ((maxPrice + minPrice) / 2) * 100 - feeArray[maxIndex] -
            feeArray[minIndex]) + "&")
        f.write(str((maxPrice - threshold - minPrice - threshold) * amount))
        f.write("\"}, ")
        f.close()

        print("TEST")

        print("printing test balance")

        dollarBalace = getBuyBalance(exchange_name=buyDetails['name'])
        coinBalance = getSellBalance(exchange_name=sellDetails['name'])

        print(dollarBalace)
        print(coinBalance)

        if dollarBalace == 1 or coinBalance == 1:
            if dollarBalace == 1:
                print("Buy side balance check failed in ", buyDetails["name"])
            else:
                print("Sell side balance check failed in ", sellDetails["name"])

            raise ExchangeException.ExchangeException(ErrorConstants.BALANCE_CHECK_ERROR)

        if sellDetails["amount"] <= coinBalance:
            print("Coins available to sell")
        else:
            print("Coins not enough")
            raise ExchangeException.ExchangeException(ErrorConstants.NO_BALANCE_ERROR)

        if float(buyDetails["amount"]) * buythreshold <= dollarBalace:
            print("Dollars available for buying coins")
        else:
            print("Dollars not enough for buying coins")
            raise ExchangeException.ExchangeException(ErrorConstants.NO_BALANCE_ERROR)

        if testMode :
            print("Not executing since in test mode!!!")

        else:
            print("Now live mode so executing!!!")
            # Starting sell
            # First always sell so that a buy fails will not cause a loss.
            sellResult = placeSellOrder(sellDetails["name"], sellDetails['coin'], sellDetails['amount'], sellthreshold)
            # If sell fails then skip the buy
            if sellResult == 1:
                print("sell failed now and hence skipping buy")
                raise ExchangeException.ExchangeException(ErrorConstants.EXECUTION_ERROR)
            else:
                placeBuyOrder(buyDetails["name"], buyDetails['coin'], buyDetails['amount'], buythreshold)


def getBotInfo():
    pass


def placeBuyOrder(exchange, coin, amount, buythreshold):
    try:

        if exchange == "Kraken":
            print("kraken buy")
            krakenApi.buy(coin="XXBTZUSD", amount=amount, limitprice=buythreshold)

        elif exchange == "Gemini":
            print("gemin buy")
            print(geminiApi.buy('btcusd', amount, buythreshold))

        elif exchange == "Gdax":
            print("gdax buy")
            print(gdaxApi.buy('BTC-USD', buythreshold, amount))

        return 0

    except Exception, ex:
        raise ExchangeException.ExchangeException(ErrorConstants.EXECUTION_ERROR)


def placeSellOrder(exchange, coin, amount, sellthreshold):
    try:
        if exchange == "Kraken":
            print("kraken sell")
            print(krakenApi.sell(coin="XXBTZUSD", amount=amount, limitprice=sellthreshold))

        elif exchange == "Gemini":
            print("gemini sell")
            print(geminiApi.sell('btcusd', amount, sellthreshold))

        elif exchange == "Gdax":
            print("gdax sell")
            gdaxApi.sell('BTC-USD', sellthreshold, amount)
        return 0

    except Exception, ex:
        raise ExchangeException.ExchangeException(ErrorConstants.EXECUTION_ERROR)


def getBuyBalance(exchange_name):
    try:
        print("inside the getbuy")
        if exchange_name == "Kraken":
            return krakenApi.getBuyBalance()


        elif exchange_name == "Gemini":
            return geminiApi.getBuyBalance()

        elif exchange_name == "Gdax":
            return gdaxApi.getBuyBalance()

        return 0

    except Exception, ex:
        raise ExchangeException.ExchangeException(ErrorConstants.BALANCE_CHECK_ERROR)


def getSellBalance(exchange_name):
    try:
        print("inside the sell")
        if exchange_name == "Kraken":
            return krakenApi.getSellBalance(Constants.bitcoin)

        elif exchange_name == "Gemini":

            return geminiApi.getSellBalance(Constants.bitcoin)

        elif exchange_name == "Gdax":

            return gdaxApi.getSellBalance(Constants.bitcoin)

        return 1

    except Exception, ex:
        raise ExchangeException.ExchangeException(ErrorConstants.BALANCE_CHECK_ERROR)


while True:

    getBotInfo()

    try:
        arbitrage()
    except Exception, ex:
        ExceptionResolver().resolve(ex)

    if (runInterval == 0):
        time.sleep(10)
    else:
        time.sleep(runInterval * 60)
