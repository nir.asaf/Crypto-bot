"""

Class that resolves the exceptions that occured in the system.

"""
import ExchangeException


class ExceptionResolver:
    def __init__(self):
        pass

    def resolve(self, CustomExceptionObj):
        """
        Function that resolves the exception that occurred.
        In future this is to be extended to handle all exceptions
        that happened

        :return (int): returns 1 after handling all the issues
        """

        # TODO: Add logging in future so that developer can troubleshoot easily
        if isinstance(CustomExceptionObj, ExchangeException.ExchangeException):
            print("Error occured in the Exchange Apis")
            print("Please contact the User Service for more resolution")
            print("Code - ", CustomExceptionObj.code)
        else:
            print("Non APi error occured, Contact Service for more resolution")
        print("continuing with the execution for the next run ... Please Wait!!!")
