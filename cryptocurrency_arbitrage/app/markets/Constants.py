"""

This file defines all the contants required for exchange modules

"""


class Constants():
    # List of supported coins for the bot
    bitcoin = "bitcoin"
    ethereum = "ethereum"
    litecoin = "litecoin"

    # List of coinfiat pairs to be used for trading in the same order as the supportedCoins
    GeminiTickSymbols = {bitcoin: "btcusd", ethereum: "ethusd"}

    # List of coins supported in Gemini Exchange
    GeminiCoinSymbols = {bitcoin: "BTC", ethereum: "ETH"}

    # List of coins supported in Gdax Exchange
    GdaxCoinSymbols = {bitcoin: "BTC", ethereum: "ETH"}

    # List of coins supported in Gdax Exchange
    # Check https://www.kraken.com/help/fees for coin pairs
    # https://support.kraken.com/hc/en-us/articles/202944246-What-currency-codes-do-you-use-
    KrakenCoinSymbols = {bitcoin: "XXBT", ethereum: "ETH"}

    # Fee schedule for exchanges
    feeKraken = 0.02
    feeGemini = 0.02
    feeGdax = 0.02

    # Fiat Currency codes for different exchanges
    KrakenUSDollar = "USD"
    GdaxUSDollar = "USD"
    GeminiUSDollar = "USD"
