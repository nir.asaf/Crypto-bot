from Exchange import Exchange
from Constants import Constants
from GDAX import *


class PoleinixApi(Exchange):

    def getTicker(self, coin):
        super(PoleinixApi, self).getTicker(coin)

    def getBuyBalance(self):
        super(PoleinixApi, self).getBuyBalance()

    def getBalance(self):
        super(PoleinixApi, self).getBalance()

    def sell(self, coin, amount, limitprice):
        super(PoleinixApi, self).sell(coin, amount, limitprice)

    def buy(self, coin, amount, limitprice):
        super(PoleinixApi, self).buy(coin, amount, limitprice)

    def getSellBalance(self, coin):
        super(PoleinixApi, self).getSellBalance(coin)

    def __init__(self):
        pass



