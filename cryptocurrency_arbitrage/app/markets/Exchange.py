"""

Exchange api which is the base class for all Exchange definitions

"""

from abc import ABCMeta, abstractmethod


class Exchange(object):

    __metaclass__ = ABCMeta

    """
    Base class for exchanges that provides basic functions for exchange access
    Attributes:
        url     Url for accessing the exchaneg
        apikey  Api key
        apisecret   Api Secret
        balance     Current Balance of the system in the exchange
    """

    @abstractmethod
    def __init__(self, apikey, apisecret, apipass=None): pass

    @abstractmethod
    def getBalance(self): pass

    @abstractmethod
    def sell(self, coin, amount, limitprice): pass

    @abstractmethod
    def buy(self, coin, amount, limitprice): pass

    @abstractmethod
    def getTicker(self, coin): pass

    @abstractmethod
    def getBuyBalance(self): pass

    @abstractmethod
    def getSellBalance(self, coin): pass
