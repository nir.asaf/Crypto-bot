from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from pymongo import MongoClient
from modules.models import *
import modules.bots as bots
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import os
import time
import datetime
from pathlib import Path
import json
from shutil import copyfile
from bson.json_util import dumps


# Create your views here.
class LoginView(TemplateView):
    template_name = "login.html"

class IndexView(TemplateView):
    template_name = "index.html"

class BitfinexView(TemplateView):
    template_name = "bitfinex.html"

class GraphView(TemplateView):
    template_name = "graph_v02.html"

class SettingsView(TemplateView):
    template_name = "settings.html"

class LiveStreamView(TemplateView):
    template_name = "live_stream.html"

class AboutView(TemplateView):
    template_name = "about.html"

class HelpView(TemplateView):
    template_name = "help.html"

class VerifyView(TemplateView):
    template_name = "verify.html"

class ChartView(TemplateView):
    template_name = "chart.html"

def login(request):
    if (request.method == 'POST'):
        postName = request.POST['name']
        postPass = request.POST['pass']

        ret = Login.sendRequest(postName, postPass)
        if (ret > 0):
            return HttpResponse('home')
        if (ret == 0):
            return HttpResponse('failed')

def register(request):
    if (request.method == 'POST'):
        regName = request.POST['name']
        regPass = request.POST['pass']
        regMail = request.POST['email']

        ret = Register.sendRequest(regName, regPass, regMail)

        if (ret == -1): return HttpResponse('failed')
        if (ret == 1):  return HttpResponse('wait')

def chksamename(request):
    if (request.method == 'POST'):
        chkName = request.POST['name']

        ret = CheckName.sendRequest(chkName)

        if (ret > 0): return HttpResponse('existed')
        if (ret == 0):  return HttpResponse('success')

def confirm(request):
    if (request.method == 'POST'):
        confName = request.POST['name']
        confPass = request.POST['pass']
        confMail = request.POST['email']
        confHash = request.POST['hash']

        if ((confName!='')&(confPass!='')&(confMail!='')&(confHash!='')):
            ret = Confirm.sendRequest(confName, confPass, confMail, confHash)
            if (ret > 0): return HttpResponse('success')

        return HttpResponse('error')

@csrf_exempt
def setParam(request):
    if (request.method == 'POST'):
        print("SetParam")
        bots.setBotLendParam(request.POST['minRate'], request.POST['minRateLonger'], request.POST['duration'], request.POST['username'], request.POST['exchange'])
        return HttpResponse('success')
@csrf_exempt
def setExchangeParam(request):
    if (request.method == 'POST'):
        print("SetParam")
        print(request.POST['apiKey'])
        Bots.setExchangeParam(request.POST['apiKey'], request.POST['secret'], request.POST['gpass'], request.POST['fee'], request.POST['username'], request.POST['ub'], request.POST['ue'], request.POST['be'], request.POST['exchange'])
        return HttpResponse('success')

def setKey(request):
    if (request.method == 'POST'):
        print(request.POST['Pass'])
        Bots.connectKey(request.POST['API'], request.POST['Secret'], request.POST['username'], request.POST['Pass'], request.POST['exchange'])
        print("SetKey")
        return HttpResponse('success')

def startBot(request):
    if (request.method == 'POST'):
        print("StartBot")
        print(request.POST['useGemini'])
        print(request.POST['testMode'])
        Bots.setRunBotParam(request.POST['username'], request.POST['useGemini'], request.POST['useKraken'], request.POST['useGdax'], request.POST['tolerance'], request.POST['opportunity'], request.POST['amount'], request.POST['runinterval'], request.POST['testMode'])
        ret = bots.makeBot(request.POST['username'], request.POST['exchange'])
        if (ret == 1):
            return HttpResponse('success')
        elif (ret == 0):
            print("Alread Activated")
            return HttpResponse('exist')

@csrf_exempt
def stopBot(request):
    if (request.method == 'POST'):
        print("StopBot")
        ts = time.time()
        bots.stopBot(request.POST['username'], request.POST['exchange'])
        postfix = datetime.datetime.fromtimestamp(ts).strftime('%b_%d_%Y_%X %z')

#        Bots.addPastBot("","","")
        src = Path(__file__).parents[1]
        src = str(src) + "/orderlist.json"

        botlogData = open(src, 'r').read()
        botlogData = "{\"orderHistory\":" + "[" + botlogData + "{}]}"
        botlogData = json.loads(botlogData)
        botlogData = botlogData['orderHistory']

        botlogData = botlogData[:-1]
        print(len(botlogData))

        profit = 0
        if ( len(botlogData) > 0 ):
            firstTransact = botlogData[0]['data']
            firstTransact = firstTransact.split('&')
            startTime = firstTransact[0]
            print(startTime)
            for idx, log in enumerate(botlogData):
                data = log['data']
                data = data.split('&')
                profit += float(data[len(data)-1])
                print(profit)
            ret = Bots.getBotParam('test')
            print(ret)
            botTime = len(botlogData) * float(ret['runinterval'])
            print(botTime)
            Bots.addPastBot(1, startTime,botTime,len(botlogData),profit)

        print(src)
        dst = Path(__file__).parents[1]
        dst = str(dst) + "/botHistory/orderlist_" + postfix + ".json"
        dst = dst.replace(':', '_')
        print(dst)
        copyfile(src, dst)
        os.remove(src)
        return HttpResponse('success')

def sendBotLog(request):
    if (request.method == 'GET'):
        print("Get BotLog Request")
#       Change Path
        
        # if ( request.GET['exchange'] == "Poloniex"):
        #     data = open('/Volumes/Backup/workspace/Rio(Python_Crypto_Lending_Bot)/poloniexlendingbot/www/botlogs/' + request.GET['username'] + '_botlog.json').read() #opens the json file and saves the raw contents        
        # elif ( request.GET['exchange'] == "Bitfinex"):
        #     data = open('/Volumes/Backup/workspace/Rio(Python_Crypto_Lending_Bot)/Bitfinexlendingbot/www/botlogs/' + request.GET['username'] + '_botlog.json').read() #opens the json file and saves the raw contents        
        
        orderListPath = Path(__file__).parents[1]
        orderListPath = str(orderListPath) + "/orderlist.json"
        print(orderListPath)
        data = ""
        if os.path.exists(orderListPath):
            data = open(orderListPath, 'r').read()
            
        else:
            open(orderListPath, 'a')
        '''

        if ( request.GET['exchange'] == "Poloniex"):
            data = open('/var/www/tradingbot/PoloniexLendingBotProject/PoloniexLendingBot/www/botlogs/' + request.GET['username'] + '_botlog.json').read() #opens the json file and saves the raw contents        
        elif ( request.GET['exchange'] == "Bitfinex"):
            data = open('/var/www/tradingbot/PoloniexLendingBotProject/BitfinexLendingBot/www/botlogs/' + request.GET['username'] + '_botlog.json').read() #opens the json file and saves the raw contents        
        '''
        
        # print(data)
        
        # print(data)
        data = "{\"orderHistory\":" + "[" + data + "{}]}"
        jsonData = json.loads(data)
        return JsonResponse(jsonData)

def sendSpeedData(request):
    if (request.method == 'GET'):
        print("Get BotLog Request")
#       Change Path
        
        pdata = open('/Volumes/Backup/workspace/Rio(Python_Crypto_Lending_Bot)/poloniexlendingbot/www/botlogs/speedTest.json').read() #opens the json file and saves the raw contents        
        bdata = open('/Volumes/Backup/workspace/Rio(Python_Crypto_Lending_Bot)/Bitfinexlendingbot/www/botlogs/speedTest.json').read() #opens the json file and saves the raw contents        
        '''

        pdata = open('/var/www/tradingbot/PoloniexLendingBotProject/PoloniexLendingBot/www/botlogs/speedTest.json').read() #opens the json file and saves the raw contents        
        bdata = open('/var/www/tradingbot/PoloniexLendingBotProject/BitfinexLendingBot/www/botlogs/speedTest.json').read() #opens the json file and saves the raw contents        
        '''
        
        data = "{\"pChartData\":" + "[" + pdata + "{}]," + "\"bChartData\":" + "[" + bdata + "{}]}"
        jsonData = json.loads(data)
        return JsonResponse(jsonData)

def getbotInfo(request):

    if (request.method == 'GET'):
        print("\n\nGet BotInfo Request\n\n")
        data = Bots.getBotParam(request.GET['username'])

        del data['_id']

        if ( bots.isBotInActive(request.GET['username'], "") == 0):
            data['botStatus'] = True
        else:
            data['botStatus'] = False
        
#        jsonData = json.loads(data) 
        return JsonResponse(data)

def getPastBot(request):
    data = Bots.getPastBot()
    data = dumps(data)
    print(dumps(data))
    # pastBotArray = []
    # for doc in data:
    #     pastBotArray.insert(0, doc)
    # print(pastBotArray)

    return HttpResponse(data)